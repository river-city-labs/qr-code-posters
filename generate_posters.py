import yaml
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import LETTER

page_width_center = 8.5/2 * inch

# read machines config file
with open("machines.yml", 'r') as reader:
    machines = yaml.safe_load(reader)

for machine in machines:
    displayName = machines[machine]["displayName"]
    url = machines[machine]["url"]
    description = machines[machine]["description"]

    # set pdf name and size. reportlab's pdf positioning is based in points with 72pts = 1 inch.
    # positioning starts at the bottom left corner of the page, kinda like an x,y plot
    canvas = Canvas(f"pdf/rcl_qr_poster_{machine}.pdf", pagesize=LETTER)

    # Poster Title
    canvas.setFont("Helvetica", 22)
    canvas.drawCentredString(page_width_center, 10 * inch, displayName)

    # Poster Description
    # ToDo: make this automatically wrap text to a new line. Currently it just goes off screen.
    canvas.setFont("Helvetica", 14)
    canvas.drawCentredString(page_width_center, 9 * inch, description)

    # QR Code
    canvas.drawInlineImage(f"img/rcl_qr_{machine}.png", 2 * inch, 4 * inch, 4.5 * inch, 4.5 * inch)

    # QR URL
    canvas.setFont("Helvetica", 18)
    canvas.drawCentredString(page_width_center, 3 * inch, f"URL: {url}")

    # 
    canvas.setFont("Helvetica", 20)
    canvas.drawCentredString(page_width_center, 2.50 * inch, f"Immediately discontinue use of this equipment if it")
    canvas.drawCentredString(page_width_center, 2.25 * inch, f"becomes unsafe, damaged, or is not working properly;") 
    canvas.drawCentredString(page_width_center, 2.00 * inch, f"notify a makerspace director.")

    # Attribution
    canvas.setFont("Helvetica", 10)
    canvas.drawCentredString(page_width_center, 0.5 * inch, f"programmatically generated via code by the wondrous Sage Peterson")

    canvas.save()

    print(f'{machine}.pdf created successfully!')
