import yaml

gitlabURL = "https://gitlab.com/river-city-labs/qr-code-posters/"
jobName = "generate_pdf"
filename = "machine-qr-codes.md"
# read machines config file
with open("machines.yml", 'r') as reader:
    machines = yaml.safe_load(reader)

# start writing wiki file
with open(filename, 'w') as f:
    f.write("# Machine QR Codes\n")
    f.write("This page is automatically updated by the Gitlab CI build. Any changes made here will be wiped out on the next build.\n\n")
    f.write(f"To add or modify qr codes to this page, please update the [machines.yml]({gitlabURL}-/blob/main/machines.yml) config file in the Gitlab project.\n\n")
    f.write("If you need access to the Gitlab group to make changes, please message Sage Peterson.\n\n")
    f.write("## Files\n")
    f.write(f"[download all files below as a single zip archive]({gitlabURL}-/jobs/artifacts/main/download?job={jobName})\n")
    f.write("| **Machine** | **QR** | **Poster** |\n")
    f.write("| --------- | -- | ------ |\n")

    # fill out markdown table
    for machine in machines:
        displayName = machines[machine]["displayName"]
        wikiurl = machines[machine]["url"]
        pdfUrl = f"{gitlabURL}-/jobs/artifacts/main/raw/pdf/rcl_qr_poster_{machine}.pdf?job={jobName}"
        imgUrl = f"{gitlabURL}-/jobs/artifacts/main/raw/img/rcl_qr_{machine}.png?job={jobName}"

        # add markdown row
        f.write(f"| [{displayName}]({wikiurl}) | [img]({imgUrl}) | [pdf]({pdfUrl}) |\n")

print(f'{filename} created successfully!')
