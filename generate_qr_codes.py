import yaml
import qrcode
from PIL import Image

logoPath = 'rcl_logo_black.png'
logo = Image.open(logoPath)

# calculate logo size
basewidth = 175
wpercent = (basewidth/float(logo.size[0]))
hsize = int((float(logo.size[1])*float(wpercent)))
logo = logo.resize((basewidth, hsize), Image.ANTIALIAS)

# read machines config file
with open("machines.yml", 'r') as reader:
    machines = yaml.safe_load(reader)

for machine in machines:
    url = machines[machine]["url"]
    description = machines[machine]["description"]

    # create qr object
    QRcode = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)

    # add url to qr
    QRcode.add_data(url)
    QRcode.make()

    # set qr colors
    QRimg = QRcode.make_image(fill_color='Black', back_color="white").convert('RGB')

    # set qr sizes
    pos = ((QRimg.size[0] - logo.size[0]) // 2, (QRimg.size[1] - logo.size[1]) // 2)
    
    # apply qr transformations
    QRimg.paste(logo, pos)

    # save the QR code
    QRimg.save(f'img/rcl_qr_{machine}.png')

    print(f'{machine}.png created successfully!')

